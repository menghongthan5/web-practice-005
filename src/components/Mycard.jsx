import React, { Component } from 'react'
import {Card, Button, Col} from 'react-bootstrap'
export default class Mycard extends Component {
    render() {
        console.log("Foods Props: ", this.props.foods); 
        return <>{
            this.props.foods.map((item, index) =>(
                <Col xs="3" key={index}> 
                <Card  style={{ width: '15rem' }}>
                <Card.Img variant="top" src={item.thumbnail} />
                <Card.Body>
                    <Card.Title>{item.name}</Card.Title>
                    <Card.Text>
                      Prices:   {item.price} $
                    </Card.Text>
                    <Button disabled={item.qty === 0} onClick={()=>this.props.onDelet(index)} variant="danger">Delet</Button>{' '}
                    <Button onClick={()=>this.props.onAdd(index)} variant="primary">Add</Button>{' '}
                    <Button variant="warning">{item.qty}</Button>
                    <h2>Total: {item.total} $</h2>
                </Card.Body>
                </Card>
                </Col>

            ))
        }</>;
    }
}
