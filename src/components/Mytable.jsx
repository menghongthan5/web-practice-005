import React from 'react'
import {Table, Button} from 'react-bootstrap'
export default function Mytable({foods, onClear}) {
    console.log("Props on func:", foods);

    let temp = foods.filter(item=>{
      return item.qty >0
    })

    return (
      <> 
      <Button onClick={onClear} variant="warning">Clear All</Button>
      <h2>{temp.length} foods</h2>

        <Table striped bordered hover>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Price</th>
      <th>Qty</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
    {temp.map((item, index) =>(
         <tr>
         <th>{index+1}</th>
         <th>{item.name}</th>
         <th>{item.qty}</th>
         <th>{item.price}</th>
         <th>{item.total}</th>
       </tr>
        ))}
  </tbody>
</Table>
</> 
    )
}
